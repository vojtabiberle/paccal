<?php

declare(strict_types=1);

namespace Infra\BinPacking\Factory;

use Api\DTO\ProductsRequest;
use Infra\BinPacking\DTO\BinBox;
use Infra\BinPacking\DTO\PackingRequest;
use Infra\BinPacking\DTO\ProductBox;

final class PackingRequestFactory
{
    /** @var string */
    private $username;

    /** @var string */
    private $apiKey;

    private $binsConfiguration = [];

    public function create(ProductsRequest $data): PackingRequest
    {
        $bins = [];
        foreach ($this->binsConfiguration as $binConf) {
            $bins[] = new BinBox(
                $binConf['id'],
                $binConf['width'],
                $binConf['height'],
                $binConf['length'],
                $binConf['maxWeight']
            );
        }

        $items = [];
        foreach ($data->getProducts() as $product) {
            $items[] = new ProductBox(
                $product->getId(),
                $product->getWidth(),
                $product->getHeight(),
                $product->getLength(),
                $product->getWeight()
            );
        }

        return new PackingRequest(
            $this->username,
            $this->apiKey,
            $bins,
            $items
        );
    }
}
