<?php

declare(strict_types=1);

namespace Infra\BinPacking;

use GuzzleHttp\Exception\RequestException;
use Infra\BinPacking\DTO\PackedResponse;
use Infra\BinPacking\DTO\PackingRequest;
use Infra\BinPacking\Factory\PackedResponseFactory;

final class Client
{
    private const PACK_METHOD = '/packer/pack';

    /** @var PackedResponseFactory */
    private $packedResponseFactory;

    /** @var \GuzzleHttp\Client */
    private $guzzleClient;

    /** @var string */
    private $country;

    /**
     * Client constructor.
     * @param string $username
     * @param string $apiKey
     * @param string $country
     */
    public function __construct(string $country)
    {
        $this->country = $country;
    }

    private function initClient()
    {
        $baseUri = 'https://'.Config::SERVERS[$this->country]['server'];
        $certPath = __DIR__.'/'.Config::SERVERS[$this->country]['pem'];

        $this->guzzleClient = new \GuzzleHttp\Client([
            'base_uri' => $baseUri,
            'cert' => $certPath,
        ]);
    }

    public function pack(PackingRequest $request): PackedResponse
    {
        if ($this->guzzleClient === null) {
            $this->initClient();
        }

        $response = $this->guzzleClient->post(
            self::PACK_METHOD,
            [
                'body' => $request->jsonSerialize(),
            ]
        );

        return $this->packedResponseFactory->create($response);
    }
}
