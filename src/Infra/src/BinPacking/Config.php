<?php

declare(strict_types=1);

namespace Infra\BinPacking;

final class Config
{
    public const EU = 'EU';

    public const SERVERS = [
        self::EU => [
            'server' => 'eu.api.3dbinpacking.com',
            'pem' => 'pem/eu.api.3dbinpacking.com',
        ],
    ];
}
