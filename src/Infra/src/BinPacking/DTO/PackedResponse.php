<?php

declare(strict_types=1);

namespace Infra\BinPacking\DTO;

final class PackedResponse
{
    /**
     *  Lost of packed items
     *
     * @var BinBox[]
     */
    private $binsPacked;

    /**
     * The list of possible errors occured in request
     * Example: [{'level'}]
     * Possible returned values:
     *      "critical" - critical error - disabling packing
     *      "warning" - warning about incorrect data in request
     *
     * @var array
     */
    private $errors;

    /**
     * Response stat
     * Possible returned values:
     *      1 - response hasn't included critical errors
     *      0 - response has included critical errors
     * critical error - the error disabling packing. It could be caused by lack of one of the required parameters
     * ("username","api_key") or incorrect value of parameter(eg. field which should contain number, contains string).
     *
     * @var integer
     */
    private $status;

    /**
     *  Items containing incorrect parameres
     *
     * @var array
     */
    private $notPackedItems;

    /**
     * Server response time.
     *
     * @var float
     */
    private $responseTime;

    /**
     * PackedResponse constructor.
     * @param BinBox[] $binsPacked
     * @param array $errors
     * @param int $status
     * @param array $notPackedItems
     * @param float $responseTime
     */
    public function __construct(array $binsPacked, array $errors, int $status, array $notPackedItems, float $responseTime)
    {
        $this->binsPacked = $binsPacked;
        $this->errors = $errors;
        $this->status = $status;
        $this->notPackedItems = $notPackedItems;
        $this->responseTime = $responseTime;
    }

    /**
     * @return BinBox[]
     */
    public function getBinsPacked(): array
    {
        return $this->binsPacked;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getWarningErrors(): array
    {
        return $this->errors['warning'];
    }

    public function getCriticalErrors(): array
    {
        return $this->errors['critical'];
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getNotPackedItems(): array
    {
        return $this->notPackedItems;
    }

    /**
     * @return float
     */
    public function getResponseTime(): float
    {
        return $this->responseTime;
    }
}
