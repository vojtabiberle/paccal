<?php

declare(strict_types=1);

namespace Infra\BinPacking\DTO;

final class BinBox implements \JsonSerializable
{
    /**
     * Container ID. It enables to identify a particular container in a packing result
     *
     * @var string
     */
    private $id;

    /**
     * Width
     *
     * @var int|float
     */
    private $w;

    /**
     * Height
     *
     * @var int|float
     */
    private $h;

    /**
     * Depth
     *
     * @var int|float
     */
    private $d;

    /**
     * Maximum weight of items in a container. Value set to "0" (zero) means there is no weight limit
     *
     * @var int|float
     */
    private $max_wg = 0;

    /**
     * BinBox constructor.
     * @param string $id
     * @param float|int $w
     * @param float|int $h
     * @param float|int $d
     * @param float|int $max_wg
     */
    public function __construct(string $id, $w, $h, $d, $max_wg)
    {
        $this->id = $id;
        $this->w = $w;
        $this->h = $h;
        $this->d = $d;
        $this->max_wg = $max_wg;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float|int
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @return float|int
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * @return float|int
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * @return float|int
     */
    public function getMaxWg()
    {
        return $this->max_wg;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'w' => $this->w,
            'h' => $this->h,
            'd' => $this->d,
            'max_wg' => $this->max_wg,
        ];
    }
}
