<?php

declare(strict_types=1);

namespace Infra\BinPacking\DTO;

final class PackingRequest implements \JsonSerializable
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var BinBox[]
     */
    private $bins;

    /**
     * @var ProductBox[]
     */
    private $items;

    /**
     * PackingRequest constructor.
     * @param string $username
     * @param string $apiKey
     * @param BinBox[] $bins
     * @param ProductBox[] $items
     */
    public function __construct(string $username, string $apiKey, array $bins, array $items)
    {
        $this->username = $username;
        $this->apiKey = $apiKey;
        $this->bins = $bins;
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return BinBox[]
     */
    public function getBins(): array
    {
        return $this->bins;
    }

    /**
     * @return ProductBox[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function jsonSerialize()
    {
        $bins = [];
        foreach ($this->bins as $bin) {
            $bins[] = $bin->jsonSerialize();
        }

        $items = [];
        foreach ($this->items as $item) {
            $items[] = $item->jsonSerialize();
        }

        return [
            'username' => $this->username,
            'api_key' => $this->apiKey,
            'bins' => $bins,
            'items' => $items,
        ];
    }
}
