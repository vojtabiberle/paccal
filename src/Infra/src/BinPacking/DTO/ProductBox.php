<?php

declare(strict_types=1);

namespace Infra\BinPacking\DTO;

final class ProductBox implements \JsonSerializable
{
    /**
     * Item ID. It enables to identify a particular item in a packing result
     *
     * @var string
     */
    private $id;

    /**
     * Width
     *
     * @var int|float
     */
    private $w;

    /**
     * Height
     *
     * @var int|float
     */
    private $h;

    /**
     * Depth
     *
     * @var int|float
     */
    private $d;

    /**
     * Item weight
     *
     * @var int|float
     */
    private $wg;

    /**
     * ProductBox constructor.
     * @param string $id
     * @param float|int $w
     * @param float|int $h
     * @param float|int $d
     * @param float|int $wg
     */
    public function __construct(string $id, $w, $h, $d, $wg)
    {
        $this->id = $id;
        $this->w = $w;
        $this->h = $h;
        $this->d = $d;
        $this->wg = $wg;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float|int
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * @return float|int
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * @return float|int
     */
    public function getD()
    {
        return $this->d;
    }

    /**
     * @return float|int
     */
    public function getWg()
    {
        return $this->wg;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'w' => $this->w,
            'h' => $this->h,
            'd' => $this->d,
            'wg' => $this->wg,
        ];
    }
}
