<?php

declare(strict_types=1);

namespace Api\Factory;

use Api\DTO\ProductBox;
use Api\DTO\ProductsRequest;

final class ProductRequestFactory
{
    public function create($data): ProductsRequest
    {
        $products = [];

        foreach ($data['products'] as $productData) {
            $products[] = new ProductBox($productData['id'], $productData['width'], $productData['height'], $productData['length'], $productData['weight']);
        }

        return new ProductsRequest($products);
    }
}
