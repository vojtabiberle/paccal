<?php

declare(strict_types=1);

namespace Api\Handler;

use Api\DTO\ProductsRequest;
use Api\Factory\CalculatedBinResponseFactory;
use Api\Validator\ProductsRequestValidator;
use Domain\Calculator;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class CalculationHandler implements RequestHandlerInterface
{
    /** @var Calculator */
    private $calculator;

    /** @var CalculatedBinResponseFactory */
    private $calculatedBinResponseFactory;

    /**
     * CalculationHandler constructor.
     * @param Calculator $calculator
     */
    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }


    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        /** @var ProductsRequest $productRequest */
        $productRequest = $request->getAttribute(ProductsRequestValidator::VALIDATED_PRODUCT_REQUEST);

        $calculatedResponse = $this->calculator->calculate($productRequest);

        return new JsonResponse(
            $this->calculatedBinResponseFactory->create($calculatedResponse)
        );
    }
}
