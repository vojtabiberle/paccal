<?php

declare(strict_types=1);

namespace Api\DTO;

final class ProductsRequest
{
    /**
     * @var ProductBox[]
     */
    private $products;

    /**
     * ProductsRequest constructor.
     * @param ProductBox[] $products
     */
    public function __construct(array $products)
    {
        $this->products = $products;
    }

    /**
     * @return ProductBox[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}
