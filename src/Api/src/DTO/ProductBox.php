<?php

declare(strict_types=1);

namespace Api\DTO;

final class ProductBox
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var float
     */
    private $width;

    /**
     * @var float
     */
    private $height;

    /**
     * @var float
     */
    private $length;

    /**
     * @var float
     */
    private $weight;

    /**
     * ProductBox constructor.
     * @param string $id
     * @param float $width
     * @param float $height
     * @param float $length
     * @param float $weight
     */
    public function __construct(string $id, float $width, float $height, float $length, float $weight)
    {
        $this->id = $id;
        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }
}
