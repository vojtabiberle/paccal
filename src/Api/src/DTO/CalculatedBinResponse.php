<?php

declare(strict_types=1);

namespace Api\DTO;

final class CalculatedBinResponse
{
    private $id;

    /**
     * CalculatedBinResponse constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}
