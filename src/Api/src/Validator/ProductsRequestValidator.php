<?php

declare(strict_types=1);

namespace Api\Validator;

use Api\Exception\MissingProductsException;
use Api\Exception\MissingRequiredProductKeyException;
use Api\Factory\ProductRequestFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class ProductsRequestValidator implements MiddlewareInterface
{
    public const VALIDATED_PRODUCT_REQUEST = 'validatedProductRequest';

    /**
     * @var ProductRequestFactory
     */
    private $productRequestFactory;

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $data = $request->getParsedBody();

        $this->checkRequestData($data);

        foreach ($data['products'] as $productData) {
            $this->checkProductData($productData);
        }

        $request = $request->withAttribute(
            self::VALIDATED_PRODUCT_REQUEST,
            $this->productRequestFactory->create($data)
        );

        $handler->handle($request);
    }

    private function checkRequestData($data): void
    {
        if (is_array($data) && !array_key_exists('products', $data) && !is_array($data['products'])) {
            throw new MissingProductsException('Missing "products" key in request or key is not array of products.');
        }
    }

    private function checkProductData($data): void
    {
        if (!array_key_exists('id', $data) && !is_string($data['id'])) {
            throw new MissingRequiredProductKeyException('Missing "id" key in product object or object is not string.');
        }

        $requiredKeys = ['width', 'height', 'length', 'weight'];

        foreach ($requiredKeys as $required) {
            if (!array_key_exists($required, $data) && !(is_int($data[$required]) || is_float($data[$required]))) {
                throw new MissingRequiredProductKeyException('Missing "' . $required . '" key in product object or object is not integer|float.');
            }
        }
    }
}
