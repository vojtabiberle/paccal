<?php

declare(strict_types=1);

namespace Api\Exception;

final class MissingRequiredProductKeyException extends \RuntimeException
{

}
