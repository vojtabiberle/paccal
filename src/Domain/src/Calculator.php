<?php

declare(strict_types=1);

namespace Domain;

use Api\DTO\CalculatedBinResponse;
use Api\DTO\ProductsRequest;
use Api\Factory\CalculatedBinResponseFactory;
use Domain\Exception\CriticalException;
use Infra\BinPacking\Client;
use Infra\BinPacking\Factory\PackingRequestFactory;

final class Calculator
{
    /** @var Client */
    private $binPackingClient;

    /** @var PackingRequestFactory */
    private $packingRequestFactory;

    /** @var CalculatedBinResponseFactory */
    private $calculatedBinResponseFactory;

    public function calculate(ProductsRequest $productsRequest): CalculatedBinResponse
    {
        //TODO: check DB cache

        $request = $this->packingRequestFactory->create($productsRequest);
        $packedResponse = $this->binPackingClient->pack($request);

        if ($packedResponse->getStatus() ===  0) {
            throw new CriticalException("Calculation failed. Reason: ".$packedResponse->getCriticalErrors());
        }

        //TODO: store into DB cache

        return $this->calculatedBinResponseFactory->create($packedResponse);
    }
}
