<?php

declare(strict_types=1);

return [
    'boxes' => [
        'small' => [
            'id' => 'small',
            'width' => '10',
            'height' => '10',
            'length' => '10',
            'maxWeight' => 10,
        ],
        'medium' => [
            'id' => 'medium',
            'width' => '30',
            'height' => '30',
            'length' => '30',
            'maxWeight' => 30,
        ],
        'large' => [
            'id' => 'large',
            'width' => '50',
            'height' => '50',
            'length' => '50',
            'maxWeight' => 50,
        ],
        'extraLarge' => [
            'id' => 'extraLarge',
            'width' => '100',
            'height' => '100',
            'length' => '100',
            'maxWeight' => 100,
        ],
    ],
];
